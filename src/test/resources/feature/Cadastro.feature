#language: en

Feature: : Cadastro do cliente

  @Servico
  Scenario: : Cadastrar um cliente no site de comprar
    Given o inicio do meu navegador com a url "http://automationpractice.com/index.php"
    When clico no botao sing in
    And preencho o campo email address com um email aleatorio
    And clico no botao create an account
    And seleciono o campo title com "Mr."
    And no campo first name preencho com "Bruno"
    And no campo last name preencho com "Souza"
    And crio um passoword
    And nos combos date of birth preencho com o dia "14" mes "November" e ano "1992"
    And marco o campo Sign up for our newsletter
    And no campo address preencho com "Para de minas 34"
    And no campo address line 2 preencho com "casa dos fundos"
    And no campo city preencho com "São Paulo"
    And no combo state seleciono a opcao "Massachusetts"
    And no campo zip postal code preencho com "01001"
    And no combo country seleciono a opcao "United States"
    And no campo addtional information escrevo "Teste de automação"
    And no campo mobile phone preencho com "11945825199"
    And no campo address alias preencho com "testandocpm2222@gmail.com"
    And clico no botao register
    Then verifico a presenca da mensagem de bem vindo "Welcome to your account"