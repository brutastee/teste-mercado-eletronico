  #Language: en

  @Servico
  Feature: Validar as regras e viriacoes na tela de login

    Scenario: Fazer o login com o mesmo usuario que foi cadastrado
      Given o inicio do meu navegador com a url "http://automationpractice.com/index.php"
      When clico no botao sing in
      And no campo email address coloco um email valido recuperando o email criado
      And no campo password coloco uma senha valida recuperando a senha criada
      And clico em sing in
      Then verifico a presenca da mensagem de bem vindo "Welcome to your account"

    Scenario: Fazer o login com um usuario invalido
      Given o inicio do meu navegador com a url "http://automationpractice.com/index.php"
      When clico no botao sing in
      And no campo email address coloco um email invalido
      And no campo password coloco uma senha invalida
      And clico em sing in
      Then verifico a presenca da mensagem de erro com a mensagem "Authentication failed."

    Scenario: Login em branco
      Given o inicio do meu navegador com a url "http://automationpractice.com/index.php"
      When clico no botao sing in
      And clico em sing in
      Then verifico a presenca da mensagem de erro com a mensagem "An email address required."