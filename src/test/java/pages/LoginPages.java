package pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utils.PageObjects;

import static hooks.DriverFactory.getDriver;

public class LoginPages extends PageObjects {

    @FindBy(id = "email")
    private WebElement inputEmailLogin;

    @FindBy(id = "passwd")
    private WebElement inputPassword;

    @FindBy(css = "[name='SubmitLogin']")
    private WebElement btnLoginSingIn;

    @FindBy(xpath = "//li[.='Authentication failed.']")
    private WebElement msgErro;

    public void noCampoEmailAddressColocoUmEmail(String email) {
        escrever(inputEmailLogin, email);
    }

    public void noCampoPasswordColocoUmaSeha(String senha) {
        escrever(inputPassword, senha);
    }

    public void clicoEmSingIn() {
        btnLoginSingIn.click();
    }

    public void verificoAPresencaDaMensagemDeErro(String msg) {
        WebElement op = getDriver().findElement(By.xpath(String.format("//li[.='%s']", msg)));
        Assert.assertEquals(msg, op.getText());
    }
}