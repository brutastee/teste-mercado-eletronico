package pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utils.PageObjects;

import java.util.Random;

import static hooks.DriverFactory.getDriver;
import static hooks.DriverFactory.wait;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOfElementLocated;

public class CadastroPages extends PageObjects {

    private static final Random random = new Random();

    //aqui fica os locator
    @FindBy(css = "[class ='login']")
    private WebElement btnLogin;

    @FindBy(css = "[name='email_create']")
    private WebElement inputEmail;

    @FindBy(xpath = "//button[normalize-space()='Create an account']")
    private WebElement btnCreateAccount;

    @FindBy(css = "[name='customer_firstname']")
    private WebElement inputFirstName;

    @FindBy(css = "[name='customer_lastname']")
    private WebElement inputLastName;

    @FindBy(css = "[name='passwd']")
    private WebElement inpuPassword;

    @FindBy(css = "[name='days']")
    private WebElement comboDays;

    @FindBy(css = "[name='months']")
    private WebElement comboMonths;

    @FindBy(css = "[name='years']")
    private WebElement comboYears;

    @FindBy(css = "[name='newsletter']")
    private WebElement checkNewslestter;

    @FindBy(css = "[name='address1']")
    private WebElement inputAddress;

    @FindBy(css = "[name='address2']")
    private WebElement inputAddressLine;

    @FindBy(css = "[name='city']")
    private WebElement inputCity;

    @FindBy(css = "[name='id_state']")
    private WebElement comboStates;

    @FindBy(css = "[name='postcode']")
    private WebElement inputZipCode;

    @FindBy(css = "[name='id_country']")
    private WebElement comboCountry;

    @FindBy(css = "[name='other']")
    private WebElement textAdditional;

    @FindBy(css = "[name='phone']")
    private WebElement inputPhone;

    @FindBy(css = "[name='alias']")
    private WebElement inputAddressAlis;

    @FindBy(css = "[name='submitAccount']")
    private WebElement btnRegister;

    @FindBy(xpath = "//p[contains(text(),'Welcome to your account.')]")
    private WebElement msgSucesso;

    //aqui fica os metodos com as suas acoes encapsulada
    public void oInicioDoMeuNavegadorComAUrl(String url) {
        getDriver().get(url);
    }

    public void clicoNoBotaoSingIn() {
        click(btnLogin);
    }

    //metodo que geral um numero aleatorio, transforma esse numero em estring e concatena com o email
    public String preenchoOCampoEmailAddressCom() {
        Random random = new Random();
        int number = random.nextInt(10000000);
        String email = number + "@desafioqa.com";
        escrever(inputEmail, email);
        return email;
    }

    public void clicoNoBotaoCreateAndAccount() {
        click(btnCreateAccount);
    }

    public void selecionoOCampoTitleCom(String opcap) {
        wait.until(visibilityOfElementLocated
                (By.xpath(String.format("//label[normalize-space()='%s']//input", opcap)))).click();
    }

    public void noCampoFirstNamePreenchoCom(String firstName) {
        escrever(inputFirstName, firstName);
    }

    public void noCampoLastNamePreenchoCom(String lastName) {
        escrever(inputLastName, lastName);
    }

    // chamo o metodo de gerar senhas aleatorias e as converto para string e passo no metodo de escrever
    public String crioUmPassword() {
        String senha = Integer.toString(geradorDeNumero(100000, 1000000));
        escrever(inpuPassword, senha);
        return senha;
    }

    public void nosCombosDateOfBirthPreenchoCom(String dia, String mes, String ano) {
        selectCombo(comboDays, dia);
        selectCombo(comboMonths, mes);
        selectCombo(comboYears, ano);
    }

    public void marcoOcampoSingUpForOurNewsletter() {
        click(checkNewslestter);
    }

    public void noCampoAddressPreenchoCom(String address) {
        escrever(inputAddress, address);
    }

    public void noCampoAddressLinePreenchoCom(String addressLine) {
        escrever(inputAddressLine, addressLine);
    }

    public void noCampoCiyPreenchoCom(String city) {
        escrever(inputCity, city);
    }

    public void noComboStateSeleciono(String states) {

        selectCombo(comboStates, states);
    }

    public void noCampoZiPostalCodePreenchoCom(String postalCode) {
        escrever(inputZipCode, postalCode);
    }

    public void noCampoCountrySelecionoAOpcao(String country) {
        selectCombo(comboCountry, country);
    }

    public void noCampoAddtionalInformationEscrevo(String optional) {
        escrever(textAdditional, optional);
    }

    public void noCampoMobilePhonePreenchoCom(String number) {
        escrever(inputPhone, number);
    }

    public void noCampoAddressAliasPreenchoCom(String address) {
        escrever(inputAddressAlis, address);
    }

    public void clicoNoBotaoRegister() {
        click(btnRegister);
    }

    public void verificoPresencaDaMensagemDeBemVindo(String mensagem) {
        String msg = msgSucesso.getText().substring(0, 23);
        Assert.assertEquals(msg, mensagem);
    }

    // esse metodo vai gerar senhas aleatorias
    public static int geradorDeNumero(int numeroInicial, int numeroFinal) {
        if (numeroInicial > numeroFinal) {
            System.out.println("o Limite inicial não pode ser supeior ao limite final");
        }
        return random.nextInt((numeroFinal - numeroInicial) + 1) + numeroInicial;
    }

}