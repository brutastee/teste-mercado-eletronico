package steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import pages.LoginPages;

import java.io.IOException;

public class LoginSteps {

    LoginPages loginPages;

    public LoginSteps(LoginPages loginPages) {
        this.loginPages = loginPages;
    }

    private static final String EMAIL_ERRADO = "naoexisto@desafioqa.com";
    private static final String PASSWORD_ERRADO = "545454";

    @And("no campo email address coloco um email valido recuperando o email criado")
    public void noCampoEmailAddressColocoUmEmailValido() throws ConfigurationException, IOException {
        PropertiesConfiguration props = loginPages.propertiesFile("login.properties");
        loginPages.noCampoEmailAddressColocoUmEmail(props.getString("valid.email"));
    }

    @And("no campo password coloco uma senha valida recuperando a senha criada")
    public void noCampoPasswordColocoUmaSehaValida() throws ConfigurationException, IOException {
        PropertiesConfiguration props = loginPages.propertiesFile("login.properties");
        loginPages.noCampoPasswordColocoUmaSeha(props.getString("valid.password"));
    }

    @And("no campo email address coloco um email invalido")
    public void noCampoEmailAdresColocoUmEmailInvalido() {
        loginPages.noCampoEmailAddressColocoUmEmail(EMAIL_ERRADO);
    }

    @And("no campo password coloco uma senha invalida")
    public void noCampoSenhaColocoUmaSenhaInvalida() {
        loginPages.noCampoPasswordColocoUmaSeha(PASSWORD_ERRADO);
    }

    @And("clico em sing in")
    public void clicoEmSingIn() {
        loginPages.clicoEmSingIn();
    }

    @Then("verifico a presenca da mensagem de erro com a mensagem {string}")
    public void verificoAPresencaDaMensagemDeErro(String msg) {
        loginPages.verificoAPresencaDaMensagemDeErro(msg);
    }
}