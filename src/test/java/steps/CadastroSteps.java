package steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import pages.CadastroPages;


import java.io.IOException;


public class CadastroSteps {

    //aqui crio uma instacia do tipo POCadastro

    CadastroPages cadastro;

    public CadastroSteps(CadastroPages cadastroPages) {
        this.cadastro = cadastroPages;
    }

    //crio um construtor que espera receber no parametro uma instancia do tipo POCadastro

    @Given("o inicio do meu navegador com a url {string}")
    public void oInicioDoMeuNavegadorComAUrl(String url) {
        cadastro.oInicioDoMeuNavegadorComAUrl(url);
    }

    @When("clico no botao sing in")
    public void clicoNoBotaoSingIn() {
        cadastro.clicoNoBotaoSingIn();
    }

    @And("preencho o campo email address com um email aleatorio")
    public void preenchoOCampoEmailAddressCom() throws ConfigurationException, IOException {
        String email = cadastro.preenchoOCampoEmailAddressCom();
        PropertiesConfiguration props = cadastro.propertiesFile("login.properties");
        props.setProperty("valid.email", email);
        props.save();
    }

    @And("clico no botao create an account")
    public void clicoNoBotaoCreateAndAccount() {
        cadastro.clicoNoBotaoCreateAndAccount();
    }

    @And("seleciono o campo title com {string}")
    public void selecionoOCampoTitleCom(String opcao) {
        cadastro.selecionoOCampoTitleCom(opcao);
    }

    @And("no campo first name preencho com {string}")
    public void noCampoFirstNamePreenchoCom(String firstName) {
        cadastro.noCampoFirstNamePreenchoCom(firstName);
    }

    @And("no campo last name preencho com {string}")
    public void noCampoLastNamePreenchoCom(String lastName) {
        cadastro.noCampoLastNamePreenchoCom(lastName);
    }

    // aqui chamo o metodo de cruiar senha e seto no meu aquivo properties e salvo
    @And("crio um passoword")
    public void crioUmPassword() throws ConfigurationException, IOException {
        String senha = cadastro.crioUmPassword();
        PropertiesConfiguration props = cadastro.propertiesFile("login.properties");
        props.setProperty("valid.password", senha);
        props.save();
    }

    @And("nos combos date of birth preencho com o dia {string} mes {string} e ano {string}")
    public void nosCombosDateOfBirthPreenchoCom(String dia, String mes, String ano) {
        cadastro.nosCombosDateOfBirthPreenchoCom(dia, mes, ano);
    }

    @And("marco o campo Sign up for our newsletter")
    public void marcoOcampoSingUpForOurNewsletter() {
        cadastro.marcoOcampoSingUpForOurNewsletter();
    }

    @And("no campo address preencho com {string}")
    public void noCampoAddressPreenchoCom(String address) {
        cadastro.noCampoAddressPreenchoCom(address);
    }

    @And("no campo address line 2 preencho com {string}")
    public void noCampoAddressLinePreenchoCom(String addressLine) {
        cadastro.noCampoAddressLinePreenchoCom(addressLine);
    }

    @And("no campo city preencho com {string}")
    public void noCampoCiyPreenchoCom(String city) {
        cadastro.noCampoCiyPreenchoCom(city);
    }

    @And("no combo state seleciono a opcao {string}")
    public void noComboStateSeleciono(String states) {
        cadastro.noComboStateSeleciono(states);
    }

    @And("no campo zip postal code preencho com {string}")
    public void noCampoZiPostalCodePreenchoCom(String postalCode) {
        cadastro.noCampoZiPostalCodePreenchoCom(postalCode);
    }

    @And("no combo country seleciono a opcao {string}")
    public void noCampoCountrySelecionoAOpcao(String country) {
        cadastro.noCampoCountrySelecionoAOpcao(country);
    }

    @And("no campo addtional information escrevo {string}")
    public void noCampoAddtionalInformationEscrevo(String optional) {
        cadastro.noCampoAddtionalInformationEscrevo(optional);
    }

    @And("no campo mobile phone preencho com {string}")
    public void noCampoMobilePhonePreenchoCom(String number) {
        cadastro.noCampoMobilePhonePreenchoCom(number);
    }

    @And("no campo address alias preencho com {string}")
    public void noCampoAddressAliasPreenchoCom(String address) {
        cadastro.noCampoAddressAliasPreenchoCom(address);
    }

    @And("clico no botao register")
    public void clicoNoBotaoRegister() {
        cadastro.clicoNoBotaoRegister();
    }

    @Then("verifico a presenca da mensagem de bem vindo {string}")
    public void verificoPresencaDaMensagemDeBemVindo(String mensagem) {
        cadastro.verificoPresencaDaMensagemDeBemVindo(mensagem);
    }

}