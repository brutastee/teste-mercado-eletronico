package runner;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

//a classe runner para poder rodas as features, onde nas tags vc pode definir quais
// features vão rodar se a feature estiver marcada com a mesma tag
//
@CucumberOptions
        (tags = "@Servico",
                features = "src/test/resources/feature",
                strict = true,
                glue = {"steps", "hooks"},
                plugin = {"pretty",
                        "json:target/cucumber-json-report/cucumber.json",
                        "de.monochromata.cucumber.report.PrettyReports:target/",
                        "timeline:target/cucumber-timeline-report/"}
        )

public class Runner extends AbstractTestNGCucumberTests {
}