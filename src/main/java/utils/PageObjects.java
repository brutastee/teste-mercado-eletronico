package utils;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;

import static hooks.DriverFactory.getDriver;
import static org.apache.commons.lang3.StringUtils.normalizeSpace;

public abstract class PageObjects {

    public PageObjects() {
        PageFactory.initElements(getDriver(), this);
    }


    protected static void aguardarElementoSerCricavel(WebElement element) {
        try {
            new WebDriverWait(getDriver(), 10)
                    .until(ExpectedConditions.elementToBeClickable(element));
        } catch (TimeoutException e) {
            e.getMessage();
        }
    }

    // metodo com acao de click
    public static void click(WebElement e) {
        aguardarElementoSerCricavel(e);
        e.click();
    }

    // metodo com acao de escrever
    public static void escrever(WebElement element, String x) {
        aguardarElementoSerCricavel(element);
        element.clear();
        element.sendKeys(normalizeSpace(x));
    }

    //metodo com acao de selecionar o item de um combo
    public static void selectCombo(WebElement e, String x) {
        e.click();
        getDriver().findElement(By.xpath(String.format("//select//option[contains(text(),'%s')]", x))).click();
    }

    public PropertiesConfiguration propertiesFile(String name) throws IOException, ConfigurationException {
        File propsFile = new File(FileUtils.getTempDirectory(), name);

        if (propsFile.createNewFile()) {
            System.out.println("foi criado o arquivo" + name);
        }

        propsFile.deleteOnExit();

        return new PropertiesConfiguration(propsFile);
    }
}