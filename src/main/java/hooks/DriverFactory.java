package hooks;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Optional;
import java.util.concurrent.TimeUnit;

public class DriverFactory {

    private static WebDriver driver;
    public static WebDriverWait wait;
    public static ChromeOptions options;

    // foi adicionado um contrutor privado para esconder o o publico estaticos
    private DriverFactory() {
    }

    //retorna a instancia do webdriver iniciada
    public static WebDriver getDriver() {
        return driver;
    }

    //esse metodo inicializa o navegador e passa as suas configurações
    public static void createBrowser() {
        String os = System.getProperty("os.name").toLowerCase();

        switch (os) {
            case "linux":
                break;
            case "windows 10":
                WebDriverManager.chromedriver().setup();
                break;
            default:
                throw new IllegalArgumentException("Driver não instanciado por favor passar o Chrome ou o FireFox");
        }
        options = new ChromeOptions();
        options.setHeadless(true);
        options.addArguments("--no-sandbox",
                "window-size=1360,768",
                "--ignore-ssl-errors",
                "--ignore-certificate-errors",
                "--disable-dev-shm-usage");
        driver = new ChromeDriver(options);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
        wait = new WebDriverWait(driver, 20);
    }

    // esses metodos devolvem o caminho executavel dos drivers
    private static String getChromeDriverFilePath() {
        return Optional.ofNullable(DriverFactory.class
                        .getClassLoader().getResource("chromedriver.exe"))
                .orElseThrow(() -> new RuntimeException("Arquivo não encontrado"))
                .getPath();
    }

    private static String getFireFoxDriverFilePath() {
        return Optional.ofNullable(DriverFactory.class.
                        getClassLoader().getResource("geckodriver.exe"))
                .orElseThrow(() -> new RuntimeException("Arquivo não encontrado"))
                .getPath();
    }

    //esse metodo encerrar o navegador
    public static void getFecharNevegador() {
        Optional.ofNullable(driver).
                ifPresent(WebDriver::quit);
    }
}