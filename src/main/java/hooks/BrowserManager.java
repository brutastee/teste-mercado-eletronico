package hooks;

import io.cucumber.java.After;
import io.cucumber.java.AfterStep;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import static hooks.DriverFactory.*;

public class BrowserManager {


    //no before eu chamo o metodo para criacao do navegador
    @Before
    public void oInicioDoMeuNavegadorComAUrl(Scenario s) {
        createBrowser();
    }

    // no after eu chamo o metodo para encerrar o navegador
    @AfterStep
    public void printCadaPasso(Scenario scn) {
        takeScreenShot(scn, "print de cada passo.png");
    }

    @After
    public void fecharNavegador(Scenario scenario) {
        if (scenario.isFailed()) {
            takeScreenShot(scenario, "print da falha.png");
        }
        getFecharNevegador();
    }


    private void takeScreenShot(Scenario scenario, String nome) {
        TakesScreenshot screenshot = (TakesScreenshot) getDriver();
        byte[] data = screenshot.getScreenshotAs(OutputType.BYTES);
        scenario.embed(data, "image/png", nome);
    }
}