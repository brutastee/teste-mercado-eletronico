# Teste Mercado Eletronico

### Requisitos para execução dos teste automatizados
-Java 8 ou superior;
-Maven 3.6.1 ou superior;

### Ferramentas Utilizadas

* IntelliJ IDEA Community Edition 2020.1.1
* Apache Maven 3.6.1
* Java SDK 8
* Selenium Java 3.149.5
* Cucumber Java 5.5.0
* Cucumber picocontainer 5.5.0
* Cucumber testng 5.5.0
* reporting-plugin 4.0.18
* junit 4.12

### Execução dos testes
Para execução dos testes de api siga os passos:

1. Execute o prompt de comando;
2. Navegue até o diretório do projeto onde se encontra o arquivo "pom.xml";
3. opção para execução dos testes
          * 3.1 - Execute o comando 'mvn clean test -Dtest=runner/Runner'
4. outra opção é executar todos os testes na classe Runner
5. Ao término da execução, o relatório será gerado no caminho 'evidencia/cucumber-reports/index.html'
